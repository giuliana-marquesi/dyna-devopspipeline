output "StagingInstance" {
  value       = module.aws.StagingInstance
  description = "The Public DNS of your Staging System"

}
output "ProductionInstance" {
  value       = module.aws.ProductionInstance
  description = "The Public DNS of your Production System"
}

output "DynatraceBuildReportEndpoint" {
  value       = module.aws.DynatraceBuildReportEndpoint
  description = "URL to access the Build Validation Results"
}

output "HandleDynatraceProblemEndpoint" {
  value       = module.aws.HandleDynatraceProblemEndpoint
  description = "Endpoint for Integrating Dynatrace Problem Notifications"
}
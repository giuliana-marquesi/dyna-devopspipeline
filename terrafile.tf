module "aws" {
  source = "./aws"

  DynatraceTenantURL     = var.DynatraceTenantURL
  DynatraceAPIToken      = var.DynatraceAPIToken
  DynatraceOneAgentURL   = var.DynatraceOneAgentURL
  DynatraceOneAgentToken = var.DynatraceOneAgentToken

  dt_key_name      = "dynatracedevops-kp"
  public_key_path  = ".ssh/id_rsa.pub"
  private_key_path = ".ssh/id_rsa"
  dt_s3            = "mybucket-dynatracedevops"
}



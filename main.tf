provider "aws" {
  region  = "us-east-2"
  version = "~> 2.0"
}

terraform {
  backend "s3" {
    dynamodb_table = "state_lock_table"
    bucket         = "terraform-dynapipeline-bucket"
    key            = "estado.tfstate"
    region         = "us-east-2"
    encrypt        = true
  }
}

resource "aws_codedeploy_deployment_group" "DeploymentGroupStaging" {
  deployment_group_name  = "Staging"
  app_name               = aws_codedeploy_app.CodeDeploy.name
  deployment_config_name = "CodeDeployDefault.OneAtATime"
  ec2_tag_filter {
    key   = "Environment"
    type  = "KEY_AND_VALUE"
    value = "DeployStaging"
  }
  deployment_style {
    deployment_type   = "IN_PLACE"
    deployment_option = "WITHOUT_TRAFFIC_CONTROL"
  }
  service_role_arn = aws_iam_role.DynatraceCodeDeployRole.arn
}

resource "aws_codedeploy_deployment_group" "DeploymentGroupStagingTests" {
  deployment_group_name  = "StagingTests"
  app_name               = aws_codedeploy_app.CodeDeploy.name
  deployment_config_name = "CodeDeployDefault.OneAtATime"
  ec2_tag_filter {
    key   = "Environment"
    type  = "KEY_AND_VALUE"
    value = "DeployStaging"
  }
  deployment_style {
    deployment_type   = "IN_PLACE"
    deployment_option = "WITHOUT_TRAFFIC_CONTROL"
  }
  service_role_arn = aws_iam_role.DynatraceCodeDeployRole.arn
}

resource "aws_codedeploy_deployment_group" "DeploymentGroupProduction" {
  deployment_group_name  = "Production"
  app_name               = aws_codedeploy_app.CodeDeploy.name
  deployment_config_name = "CodeDeployDefault.OneAtATime"
  ec2_tag_filter {
    key   = "Environment"
    type  = "KEY_AND_VALUE"
    value = "DeployProduction"
  }
  deployment_style {
    deployment_type   = "IN_PLACE"
    deployment_option = "WITHOUT_TRAFFIC_CONTROL"
  }
  service_role_arn = aws_iam_role.DynatraceCodeDeployRole.arn
}

resource "aws_codedeploy_deployment_group" "DeploymentGroupProductionTests" {
  deployment_group_name  = "ProductionTests"
  app_name               = aws_codedeploy_app.CodeDeploy.name
  deployment_config_name = "CodeDeployDefault.OneAtATime"
  ec2_tag_filter {
    key   = "Environment"
    type  = "KEY_AND_VALUE"
    value = "DeployProduction"
  }
  deployment_style {
    deployment_type   = "IN_PLACE"
    deployment_option = "WITHOUT_TRAFFIC_CONTROL"
  }
  service_role_arn = aws_iam_role.DynatraceCodeDeployRole.arn
}

resource "aws_codedeploy_app" "CodeDeploy" {
  name = "SampleDevOpsApp"
  depends_on = [
    aws_instance.StagingInstance,
    aws_instance.ProductionInstance
  ]
}

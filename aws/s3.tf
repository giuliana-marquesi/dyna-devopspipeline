resource "aws_s3_bucket" "bucket" {
  bucket = var.dt_s3
  acl    = "private"

  versioning {
    enabled = true
  }

  tags = {
    Name = "dynatacedevops"
  }
}

resource "aws_s3_bucket" "CodePipelineOutputBucket" {
  bucket        = "codepipeline-artifacts-${aws_s3_bucket.bucket.bucket}"
  acl           = "private"
  force_destroy = true

  versioning {
    enabled = true
  }
}

resource "aws_s3_bucket_object" "appzip" {
  bucket = aws_s3_bucket.bucket.bucket
  key    = "app.zip"
  source = var.dt_bucket_object_path["appzip"]
}

resource "aws_s3_bucket_object" "lambdaszip" {
  bucket = aws_s3_bucket.bucket.bucket
  key    = "lambdas.zip"
  source = var.dt_bucket_object_path["lambdaszip"]
}

resource "aws_s3_bucket_object" "monspecjson" {
  bucket = aws_s3_bucket.bucket.bucket
  key    = "monspec.json"
  source = var.dt_bucket_object_path["monspecjson"]
}

resource "aws_s3_bucket_object" "playbookyaml" {
  bucket = aws_s3_bucket.bucket.bucket
  key    = "playbook.yaml"
  source = var.dt_bucket_object_path["playbookyaml"]
}

resource "aws_s3_bucket_object" "testszip" {
  bucket = aws_s3_bucket.bucket.bucket
  key    = "tests.zip"
  source = var.dt_bucket_object_path["testszip"]
}
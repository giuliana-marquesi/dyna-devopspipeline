resource "aws_security_group" "enable_http_ssh" {
  name        = "Dynatrace DevOps Tutorial Security Group"
  description = "Enable HTTP access via port 80 and SSH access."
}

resource "aws_security_group_rule" "enable_http_ssh" {
  cidr_blocks = [
    "0.0.0.0/0",
  ]
  from_port         = 80
  protocol          = "tcp"
  security_group_id = aws_security_group.enable_http_ssh.id
  self              = false
  to_port           = 80
  type              = "ingress"
}

resource "aws_security_group_rule" "enable_http_ssh-1" {
  cidr_blocks = [
    "0.0.0.0/0",
  ]
  from_port         = 22
  protocol          = "tcp"
  security_group_id = aws_security_group.enable_http_ssh.id

  self    = false
  to_port = 22
  type    = "ingress"
}

resource "aws_security_group_rule" "enable_http_ssh-2" {
  cidr_blocks = [
    "0.0.0.0/0",
  ]
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.enable_http_ssh.id
  self              = false
  to_port           = 0
  type              = "egress"
}

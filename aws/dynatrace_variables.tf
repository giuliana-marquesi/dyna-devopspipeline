# variables from Dynatrace

variable "DynatraceTenantURL" {
  type        = string
  description = "Your full Dynatrace Tenant URL. For SaaS it is e.g: https://<YOURTENANTID>.live.dynatrace.com. For Managed something like https://yourserver/e/abcdefgh-1234-5678-abcd-55a7265f572f"
}

variable "DynatraceAPIToken" {
  type        = string
  description = "Your Dynatrace API Token. If you dont have one create one first in your Dynatrace Web UI via Settings -> Integration -> Dynatrace API"
}

variable "DynatraceOneAgentURL" {
  type        = string
  description = "Full Download Script URL. Go to Deploy Dynatrace -> Start Installation -> Linux and copy JUST the URL in the wget command and PASTE it here"
}

variable "DynatraceOneAgentToken" {
  type        = string
  description = "Installation Token. Go to Deploy Dynatrace -> Start Installation -> Linux and copy JUST Api-Token in the wget command and PAST it here"
}

resource "aws_dynamodb_table" "BuildValidationRequests" {
  name           = "BuildValidationRequests"
  read_capacity  = 10
  write_capacity = 10

  attribute {
    name = "PipelineName"
    type = "S"
  }

  attribute {
    name = "Timestamp"
    type = "N"
  }

  hash_key  = "PipelineName"
  range_key = "Timestamp"
}
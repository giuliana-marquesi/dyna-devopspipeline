resource "aws_lambda_function" "handleDynatraceProblemNotification" {
  function_name = "handleDynatraceProblemNotification"
  handler       = "handleDynatraceProblemNotification.handler"
  runtime       = "nodejs10.x"
  timeout       = 120
  role          = aws_iam_role.DynatraceLambdaRole.arn
  s3_bucket     = aws_s3_bucket.bucket.bucket
  s3_key        = aws_s3_bucket_object.lambdaszip.key

  tracing_config {
    mode = "PassThrough"
  }
}

resource "aws_lambda_function" "getBuildValidationResults" {
  function_name = "getBuildValidationResults"
  handler       = "getBuildValidationResults.handler"
  runtime       = "nodejs10.x"
  timeout       = 120
  role          = aws_iam_role.DynatraceLambdaRole.arn
  s3_bucket     = aws_s3_bucket.bucket.bucket
  s3_key        = aws_s3_bucket_object.lambdaszip.key

  tracing_config {
    mode = "PassThrough"
  }
}

resource "aws_lambda_function" "getDynatraceBuildValidationResults" {
  function_name = "getDynatraceBuildValidationResults"
  handler       = "getDynatraceBuildValidationResults.handler"
  runtime       = "nodejs10.x"
  timeout       = 120
  role          = aws_iam_role.DynatraceLambdaRole.arn
  s3_bucket     = aws_s3_bucket.bucket.bucket
  s3_key        = aws_s3_bucket_object.lambdaszip.key

  tracing_config {
    mode = "PassThrough"
  }
}

resource "aws_lambda_function" "pushDynatraceDeploymentEvent" {
  function_name = "pushDynatraceDeploymentEvent"
  handler       = "pushDynatraceDeploymentEvent.handler"
  runtime       = "nodejs10.x"
  timeout       = 120
  role          = aws_iam_role.DynatraceLambdaRole.arn
  s3_bucket     = aws_s3_bucket.bucket.bucket
  s3_key        = aws_s3_bucket_object.lambdaszip.key

  tracing_config {
    mode = "PassThrough"
  }
}

resource "aws_lambda_function" "registerDynatraceBuildValidation" {
  function_name = "registerDynatraceBuildValidation"
  handler       = "registerDynatraceBuildValidation.handler"
  runtime       = "nodejs10.x"
  timeout       = 120
  role          = aws_iam_role.DynatraceLambdaRole.arn
  s3_bucket     = aws_s3_bucket.bucket.bucket
  s3_key        = aws_s3_bucket_object.lambdaszip.key

  tracing_config {
    mode = "PassThrough"
  }
}

resource "aws_lambda_function" "validateBuildDynatraceWorker" {
  function_name = "validateBuildDynatraceWorker"
  handler       = "validateBuildDynatraceWorker.handler"
  runtime       = "nodejs10.x"
  timeout       = 120
  role          = aws_iam_role.DynatraceLambdaRole.arn
  s3_bucket     = aws_s3_bucket.bucket.bucket
  s3_key        = aws_s3_bucket_object.lambdaszip.key

  tracing_config {
    mode = "PassThrough"
  }
}

resource "aws_lambda_permission" "LambdaInvokePermissionGetBuildValidationResultsFunction" {
  function_name = aws_lambda_function.getBuildValidationResults.arn
  action        = "lambda:InvokeFunction"
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_api_gateway_rest_api.DynatraceRestAPI.execution_arn}/*/*/*"
}

resource "aws_lambda_permission" "LambdaInvokePermissionHandleDynatraceProblemNotificationFunction" {
  function_name = aws_lambda_function.handleDynatraceProblemNotification.arn
  action        = "lambda:InvokeFunction"
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_api_gateway_rest_api.DynatraceRestAPI.execution_arn}/*/*/*"
}

resource "aws_lambda_permission" "PermissionForEventsToInvokeLambda" {
  function_name = aws_lambda_function.validateBuildDynatraceWorker.arn
  action        = "lambda:InvokeFunction"
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.ScheduleRule.arn
}

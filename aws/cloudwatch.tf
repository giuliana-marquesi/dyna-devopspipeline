resource "aws_cloudwatch_event_rule" "ScheduleRule" {
  name                = "DynatraceBuildValidationWorkerRule"
  description         = "Scan for pending Dynatrace Build Validation Requests"
  schedule_expression = "rate(2 minutes)"
  is_enabled          = true
}

resource "aws_cloudwatch_event_target" "ScheduleRule" {
  target_id = "TargetFunctionV1"
  rule      = aws_cloudwatch_event_rule.ScheduleRule.name
  arn       = aws_lambda_function.validateBuildDynatraceWorker.arn
}

variable "dt_key_name" {
  type = string
}

variable "public_key_path" {
  type = string
}

variable "private_key_path" {
  type = string
}

variable "dt_s3" {
  type = string

}

variable "dt_bucket_object_path" {

  default = {
    appzip       = "aws/copytos3/app.zip"
    lambdaszip   = "aws/copytos3/lambdas.zip"
    monspecjson  = "aws/copytos3/monspec.json"
    playbookyaml = "aws/copytos3/playbook.yaml"
    testszip     = "aws/copytos3/tests.zip"
  }
}

variable "InstanceType" {
  type        = string
  description = "Instance Types for the EC2 Instances we create for Staging and Production"
  default     = "t3.micro"
}

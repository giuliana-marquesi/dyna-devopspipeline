resource "aws_key_pair" "terraform_resource_kp" {
  key_name   = var.dt_key_name
  public_key = file(var.public_key_path)

  tags = {
    Name = "dynatracedevops"
  }
}

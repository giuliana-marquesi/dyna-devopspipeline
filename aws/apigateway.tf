resource "aws_api_gateway_rest_api" "DynatraceRestAPI" {
  name        = "Dynatrace DevOps Data API"
  description = "Access to Dynatrace Data"
}

resource "aws_api_gateway_deployment" "RestApiDeployment" {
  rest_api_id = aws_api_gateway_rest_api.DynatraceRestAPI.id
  stage_name  = "v1"
  depends_on = [
    aws_api_gateway_integration.BuildValidationResultsIntegration,
    aws_api_gateway_integration.HandleDynatraceProblemIntegration
  ]
}


#BuildValidationResults
resource "aws_api_gateway_resource" "BuildValidationResultsResource" {
  rest_api_id = aws_api_gateway_rest_api.DynatraceRestAPI.id
  parent_id   = aws_api_gateway_rest_api.DynatraceRestAPI.root_resource_id
  path_part   = "BuildValidationResults"
}

resource "aws_api_gateway_method" "BuildValidationResultsGET" {
  http_method   = "GET"
  resource_id   = aws_api_gateway_resource.BuildValidationResultsResource.id
  rest_api_id   = aws_api_gateway_rest_api.DynatraceRestAPI.id
  authorization = "NONE"
}

resource "aws_api_gateway_method_response" "BVRresponse_200" {
  rest_api_id = aws_api_gateway_rest_api.DynatraceRestAPI.id
  resource_id = aws_api_gateway_resource.BuildValidationResultsResource.id
  http_method = aws_api_gateway_method.BuildValidationResultsGET.http_method
  status_code = 200
  response_models = {
    "text/html" = "Empty"
  }
}

resource "aws_api_gateway_integration" "BuildValidationResultsIntegration" {
  type                    = "AWS_PROXY"
  passthrough_behavior    = "WHEN_NO_MATCH"
  integration_http_method = "POST"
  http_method             = aws_api_gateway_method.BuildValidationResultsGET.http_method
  resource_id             = aws_api_gateway_resource.BuildValidationResultsResource.id
  rest_api_id             = aws_api_gateway_rest_api.DynatraceRestAPI.id
  uri                     = aws_lambda_function.getBuildValidationResults.invoke_arn
}

resource "aws_api_gateway_integration_response" "BVRresponse_200" {
  rest_api_id = aws_api_gateway_integration.BuildValidationResultsIntegration.rest_api_id
  resource_id = aws_api_gateway_integration.BuildValidationResultsIntegration.resource_id
  http_method = aws_api_gateway_integration.BuildValidationResultsIntegration.http_method
  status_code = aws_api_gateway_method_response.BVRresponse_200.status_code
}

#HandleDynatraceProblem
resource "aws_api_gateway_resource" "HandleDynatraceProblemResource" {
  rest_api_id = aws_api_gateway_rest_api.DynatraceRestAPI.id
  parent_id   = aws_api_gateway_rest_api.DynatraceRestAPI.root_resource_id
  path_part   = "HandleDynatraceProblem"
}

resource "aws_api_gateway_method" "HandleDynatraceProblemPOST" {
  http_method   = "POST"
  resource_id   = aws_api_gateway_resource.HandleDynatraceProblemResource.id
  rest_api_id   = aws_api_gateway_rest_api.DynatraceRestAPI.id
  authorization = "NONE"
}

resource "aws_api_gateway_method_response" "HDPresponse_200" {
  rest_api_id = aws_api_gateway_rest_api.DynatraceRestAPI.id
  resource_id = aws_api_gateway_resource.HandleDynatraceProblemResource.id
  http_method = aws_api_gateway_method.HandleDynatraceProblemPOST.http_method
  status_code = 200
  response_models = {
    "text/html" = "Empty"
  }
}

resource "aws_api_gateway_integration" "HandleDynatraceProblemIntegration" {
  type                    = "AWS_PROXY"
  passthrough_behavior    = "WHEN_NO_MATCH"
  integration_http_method = "POST"
  http_method             = aws_api_gateway_method.HandleDynatraceProblemPOST.http_method
  resource_id             = aws_api_gateway_resource.HandleDynatraceProblemResource.id
  rest_api_id             = aws_api_gateway_rest_api.DynatraceRestAPI.id
  uri                     = aws_lambda_function.handleDynatraceProblemNotification.invoke_arn
}

resource "aws_api_gateway_integration_response" "HDPresponse_200" {
  rest_api_id = aws_api_gateway_integration.HandleDynatraceProblemIntegration.rest_api_id
  resource_id = aws_api_gateway_integration.HandleDynatraceProblemIntegration.resource_id
  http_method = aws_api_gateway_integration.HandleDynatraceProblemIntegration.http_method
  status_code = aws_api_gateway_method_response.HDPresponse_200.status_code
}

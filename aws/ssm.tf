resource "aws_ssm_parameter" "SSMSelfHealingURL" {
  name        = "DT_SELF_HEALING_URL"
  description = "Dynatrace Self Healing URL"
  value       = "https://${aws_api_gateway_resource.HandleDynatraceProblemResource.rest_api_id}.execute-api.us-east-2.amazonaws.com/v1${aws_api_gateway_resource.HandleDynatraceProblemResource.path}"
  type        = "String"
}

resource "aws_ssm_parameter" "SSMParamDtTenantTUrl" {
  name        = "DT_TENANT_URL"
  description = "Dynatrace Tenant URL including your Environment in case you run managed"
  value       = var.DynatraceTenantURL
  type        = "String"
}

resource "aws_ssm_parameter" "SSMParamDtApiToken" {
  name        = "DT_API_TOKEN"
  description = "Dynatrace API Token"
  value       = var.DynatraceAPIToken
  type        = "String"
}

resource "aws_ssm_parameter" "SSMBuildReportURL" {
  name        = "DT_BUILD_REPORT_URL"
  description = "Dynatrace Build Report URL"
  value       = "https://${aws_api_gateway_resource.BuildValidationResultsResource.rest_api_id}.execute-api.us-east-2.amazonaws.com/v1${aws_api_gateway_resource.BuildValidationResultsResource.path}"
  type        = "String"
}
output "StagingInstance" {
  value       = aws_instance.StagingInstance.public_dns
  description = "The Public DNS of your Staging System"

}
output "ProductionInstance" {
  value       = aws_instance.ProductionInstance.public_dns
  description = "The Public DNS of your Production System"
}

output "DynatraceBuildReportEndpoint" {
  value       = aws_ssm_parameter.SSMBuildReportURL.value
  description = "URL to access the Build Validation Results"
}

output "HandleDynatraceProblemEndpoint" {
  value       = aws_ssm_parameter.SSMSelfHealingURL.value
  description = "Endpoint for Integrating Dynatrace Problem Notifications"
}

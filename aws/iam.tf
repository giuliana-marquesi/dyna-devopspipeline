# DynatraceCodeDeployRole
resource "aws_iam_role" "DynatraceCodeDeployRole" {
  name               = "DynatraceCodeDeployRole"
  assume_role_policy = data.aws_iam_policy_document.DynatraceCodeDeployRole.json
  path               = "/"
}

resource "aws_iam_role_policy_attachment" "DynatraceCodeDeployRole" {
  role       = aws_iam_role.DynatraceCodeDeployRole.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSCodeDeployRole"
}


#DynatraceCodePipelineRole
resource "aws_iam_role" "DynatraceCodePipelineRole" {
  name               = "DynatraceCodePipelineRole"
  assume_role_policy = data.aws_iam_policy_document.DynatraceCodePipelineRole.json
  path               = "/"
}

resource "aws_iam_role_policy" "DynatraceEC2CodeDeployInstanceProfilePolicy" {
  name   = "DynatraceEC2CodeDeployInstanceProfilePolicy"
  role   = aws_iam_role.DynatraceCodePipelineRole.id
  policy = data.aws_iam_policy_document.DynatraceEC2CodeDeployInstanceProfilePolicy.json
}


# DynatraceEC2InstanceProfileRole
resource "aws_iam_role" "DynatraceEC2InstanceProfileRole" {
  name               = "DynatraceEC2InstanceProfileRole"
  assume_role_policy = data.aws_iam_policy_document.DynatraceEC2InstanceProfileRole.json
  path               = "/"
}

resource "aws_iam_role_policy" "DynatraceEC2CodeDeployInstanceProfilePolicy_EC2InstanceProfileRole" {
  name   = "DynatraceEC2CodeDeployInstanceProfilePolicy"
  role   = aws_iam_role.DynatraceEC2InstanceProfileRole.id
  policy = data.aws_iam_policy_document.DynatraceEC2CodeDeployInstanceProfilePolicy_EC2InstanceProfileRole.json
}

resource "aws_iam_instance_profile" "DynatraceEC2InstanceProfile" {
  name = "DynatraceEC2InstanceProfile"
  path = "/"
  role = aws_iam_role.DynatraceEC2InstanceProfileRole.name
}


# DynatraceLambdaRole
resource "aws_iam_role" "DynatraceLambdaRole" {
  name               = "DynatraceLambdaRole"
  assume_role_policy = data.aws_iam_policy_document.DynatraceLambdaRole.json
  path               = "/"
}

resource "aws_iam_role_policy_attachment" "DynatraceLambdaRoleAWSCodePipelineApproverAccess" {
  role       = aws_iam_role.DynatraceLambdaRole.name
  policy_arn = "arn:aws:iam::aws:policy/AWSCodePipelineApproverAccess"
}

resource "aws_iam_role_policy_attachment" "DynatraceLambdaRoleAmazonS3FullAccess" {
  role       = aws_iam_role.DynatraceLambdaRole.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_role_policy_attachment" "DynatraceLambdaRoleAmazonDynamoDBFullAccess" {
  role       = aws_iam_role.DynatraceLambdaRole.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

resource "aws_iam_role_policy_attachment" "DynatraceLambdaRoleAWSCodeDeployFullAccess" {
  role       = aws_iam_role.DynatraceLambdaRole.name
  policy_arn = "arn:aws:iam::aws:policy/AWSCodeDeployFullAccess"
}

resource "aws_iam_role_policy_attachment" "DynatraceLambdaRoleCloudWatchLogsFullAccess" {
  role       = aws_iam_role.DynatraceLambdaRole.name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchLogsFullAccess"
}

resource "aws_iam_role_policy_attachment" "DynatraceLambdaRoleAmazonSSMFullAccess" {
  role       = aws_iam_role.DynatraceLambdaRole.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMFullAccess"
}

resource "aws_iam_role_policy_attachment" "DynatraceLambdaRoleAWSCodePipelineCustomActionAccess" {
  role       = aws_iam_role.DynatraceLambdaRole.name
  policy_arn = "arn:aws:iam::aws:policy/AWSCodePipelineCustomActionAccess"
}


# data
data "aws_iam_policy_document" "DynatraceCodeDeployRole" {
  statement {
    actions = [
      "sts:AssumeRole"
    ]

    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["codedeploy.amazonaws.com"]
    }
  }
  version = "2012-10-17"
}

data "aws_iam_policy_document" "DynatraceCodePipelineRole" {
  statement {
    actions = [
      "sts:AssumeRole"
    ]

    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["codepipeline.amazonaws.com"]
    }
  }
  version = "2012-10-17"
}

data "aws_iam_policy_document" "DynatraceEC2InstanceProfileRole" {
  statement {
    actions = [
      "sts:AssumeRole"
    ]

    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
  version = "2012-10-17"
}

data "aws_iam_policy_document" "DynatraceLambdaRole" {
  statement {
    actions = [
      "sts:AssumeRole"
    ]

    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
  version = "2012-10-17"
}

data "aws_iam_policy_document" "DynatraceEC2CodeDeployInstanceProfilePolicy" {
  statement {
    actions = [
      "s3:GetObject",
      "s3:GetObjectVersion",
      "s3:GetBucketVersioning",
    ]
    resources = [
      "*",
    ]
    effect = "Allow"
  }

  statement {
    actions = [
      "s3:PutObject"
    ]
    resources = [
      "arn:aws:s3:::codepipeline*",
      "arn:aws:s3:::elasticbeanstalk*"
    ]
    effect = "Allow"
  }

  statement {
    actions = [
      "codecommit:CancelUploadArchive",
      "codecommit:GetBranch",
      "codecommit:GetCommit",
      "codecommit:GetUploadArchiveStatus",
      "codecommit:UploadArchive"
    ]
    resources = [
      "*"
    ]
    effect = "Allow"
  }

  statement {
    actions = [
      "codedeploy:GetApplicationRevision",
      "codedeploy:CreateDeployment",
      "codedeploy:GetDeployment",
      "codedeploy:GetDeploymentConfig",
      "codedeploy:RegisterApplicationRevision"
    ]
    resources = [
      "*"
    ]
    effect = "Allow"
  }

  statement {
    actions = [
      "elasticbeanstalk:*",
      "ec2:*",
      "elasticloadbalancing:*",
      "autoscaling:*",
      "cloudwatch:*",
      "s3:*",
      "sns:*",
      "cloudformation:*",
      "rds:*",
      "sqs:*",
      "ecs:*",
      "iam:PassRole"
    ]
    resources = [
      "*"
    ]
    effect = "Allow"
  }

  statement {
    actions = [
      "lambda:InvokeFunction",
      "lambda:ListFunctions"
    ]
    resources = [
      "*"
    ]
    effect = "Allow"
  }

  statement {
    actions = [
      "opsworks:*"
    ]
    resources = [
      "*"
    ]
    effect = "Allow"
  }

  statement {
    actions = [
      "cloudformation:*",
      "iam:PassRole"
    ]
    resources = [
      "*"
    ]
    effect = "Allow"
  }

  statement {
    actions = [
      "codebuild:*"
    ]
    resources = [
      "*"
    ]
    effect = "Allow"
  }
}

data "aws_iam_policy_document" "DynatraceEC2CodeDeployInstanceProfilePolicy_EC2InstanceProfileRole" {
  statement {
    actions = [
      "s3:Get*",
      "s3:List*"
    ]
    resources = [
      "*"
    ]
    effect = "Allow"
  }
}
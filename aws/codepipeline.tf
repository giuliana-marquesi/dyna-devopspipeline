resource "aws_codepipeline" "CodePipeline" {
  name     = "SampleDevOpsPipeline"
  role_arn = aws_iam_role.DynatraceCodePipelineRole.arn
  artifact_store {
    type     = "S3"
    location = aws_s3_bucket.CodePipelineOutputBucket.bucket
  }
  stage {
    name = "Source"

    action {
      category = "Source"
      configuration = {
        "PollForSourceChanges" = "false"
        "S3Bucket"             = aws_s3_bucket.bucket.bucket
        "S3ObjectKey"          = aws_s3_bucket_object.appzip.key
      }
      name = "Source"
      output_artifacts = [
        aws_codedeploy_app.CodeDeploy.name
      ]
      owner     = "AWS"
      provider  = "S3"
      run_order = 1
      version   = "1"
    }
    action {
      category = "Source"
      configuration = {
        "PollForSourceChanges" = "false"
        "S3Bucket"             = aws_s3_bucket.bucket.bucket
        "S3ObjectKey"          = aws_s3_bucket_object.testszip.key
      }
      name = "SourceTests"
      output_artifacts = [
        "${aws_codedeploy_app.CodeDeploy.name}Tests"
      ]
      owner     = "AWS"
      provider  = "S3"
      run_order = 1
      version   = "1"
    }
    action {
      category = "Source"
      configuration = {
        "PollForSourceChanges" = "false"
        "S3Bucket"             = aws_s3_bucket.bucket.bucket
        "S3ObjectKey"          = aws_s3_bucket_object.monspecjson.key
      }
      name = "SourceMonspec"
      output_artifacts = [
        "Monspec",
      ]
      owner     = "AWS"
      provider  = "S3"
      run_order = 1
      version   = "1"
    }
  }
  stage {
    name = "Staging"

    action {
      category = "Deploy"
      configuration = {
        "ApplicationName"     = aws_codedeploy_app.CodeDeploy.name
        "DeploymentGroupName" = aws_codedeploy_deployment_group.DeploymentGroupStaging.deployment_group_name
      }
      input_artifacts = [
        aws_codedeploy_app.CodeDeploy.name
      ]
      name      = "DeployInStaging"
      owner     = "AWS"
      provider  = "CodeDeploy"
      run_order = 1
      version   = "1"
    }
    action {
      category = "Deploy"
      configuration = {
        "ApplicationName"     = aws_codedeploy_app.CodeDeploy.name
        "DeploymentGroupName" = aws_codedeploy_deployment_group.DeploymentGroupStagingTests.deployment_group_name
      }
      input_artifacts = [
        "${aws_codedeploy_app.CodeDeploy.name}Tests"
      ]
      name      = "DeployTestsInStaging"
      owner     = "AWS"
      provider  = "CodeDeploy"
      run_order = 2
      version   = "1"
    }
    action {
      category = "Invoke"
      configuration = {
        "FunctionName"   = aws_lambda_function.pushDynatraceDeploymentEvent.function_name
        "UserParameters" = "Staging,CodePipeline Deploying in Staging"
      }
      input_artifacts = [
        "Monspec"
      ]
      name      = aws_lambda_function.pushDynatraceDeploymentEvent.function_name
      owner     = "AWS"
      provider  = "Lambda"
      run_order = 2
      version   = "1"
    }
    action {
      category = "Invoke"
      configuration = {
        "FunctionName"   = aws_lambda_function.registerDynatraceBuildValidation.function_name
        "UserParameters" = "StagingToProduction,5,ApproveStaging"
      }
      input_artifacts = [
        "Monspec"
      ]
      name      = aws_lambda_function.registerDynatraceBuildValidation.function_name
      owner     = "AWS"
      provider  = "Lambda"
      run_order = 2
      version   = "1"
    }
  }
  stage {
    name = "ApproveStaging"

    action {
      category  = "Approval"
      name      = "ApproveStaging"
      owner     = "AWS"
      provider  = "Manual"
      run_order = 1
      version   = "1"
    }
  }
  stage {
    name = "Production"

    action {
      category = "Deploy"
      configuration = {
        "ApplicationName"     = aws_codedeploy_app.CodeDeploy.name
        "DeploymentGroupName" = aws_codedeploy_deployment_group.DeploymentGroupProduction.deployment_group_name
      }
      input_artifacts = [
        aws_codedeploy_app.CodeDeploy.name
      ]
      name      = "DeployInProduction"
      owner     = "AWS"
      provider  = "CodeDeploy"
      run_order = 1
      version   = "1"
    }
    action {
      category = "Deploy"
      configuration = {
        "ApplicationName"     = aws_codedeploy_app.CodeDeploy.name
        "DeploymentGroupName" = aws_codedeploy_deployment_group.DeploymentGroupProductionTests.deployment_group_name
      }
      input_artifacts = [
        "${aws_codedeploy_app.CodeDeploy.name}Tests"
      ]
      name      = "DeployTestsInProduction"
      owner     = "AWS"
      provider  = "CodeDeploy"
      run_order = 2
      version   = "1"
    }
    action {
      category = "Invoke"
      configuration = {
        "FunctionName"   = aws_lambda_function.pushDynatraceDeploymentEvent.function_name
        "UserParameters" = "Production,CodePipeline Deploying in Production"
      }
      input_artifacts = [
        "Monspec"
      ]
      name      = aws_lambda_function.pushDynatraceDeploymentEvent.function_name
      owner     = "AWS"
      provider  = "Lambda"
      run_order = 2
      version   = "1"
    }
    action {
      category = "Invoke"
      configuration = {
        "FunctionName"   = aws_lambda_function.registerDynatraceBuildValidation.function_name
        "UserParameters" = "ProductionToProductionLastHour,5,ApproveProduction"
      }
      input_artifacts = [
        "Monspec"
      ]
      name      = aws_lambda_function.registerDynatraceBuildValidation.function_name
      owner     = "AWS"
      provider  = "Lambda"
      run_order = 2
      version   = "1"
    }
  }
  stage {
    name = "ApproveProduction"

    action {
      category  = "Approval"
      name      = "ApproveProduction"
      owner     = "AWS"
      provider  = "Manual"
      run_order = 1
      version   = "1"
    }
  }
}

resource "aws_instance" "StagingInstance" {
  ami                  = "ami-f63b1193"
  instance_type        = var.InstanceType
  key_name             = aws_key_pair.terraform_resource_kp.key_name
  iam_instance_profile = aws_iam_instance_profile.DynatraceEC2InstanceProfile.name
  security_groups = [
    aws_security_group.enable_http_ssh.name
  ]
  tags = {
    Environment = "DeployStaging"
    Name        = "Staging"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum update -y && sudo yum install -y  httpd ruby wget",
      "wget  -O /tmp/Dynatrace-OneAgent-Linux.sh '${var.DynatraceOneAgentURL}' --header='Authorization: Api-Token ${var.DynatraceOneAgentToken}'",
      "sudo /bin/sh /tmp/Dynatrace-OneAgent-Linux.sh --set-app-log-content-access=true --set-infra-only=false",
      "curl --silent --location https://rpm.nodesource.com/setup_10.x | sudo bash -",
      "sudo yum install -y nodejs",
      "sudo npm install pm2@latest -g",
      "cd /home/ec2-user",
      "export DT_CUSTOM_PROP=DEPLOYMENT_ID=123456 DEPLOYMENT_GROUP_NAME=GROUP_NAME APPLICATION_NAME=APP_NAME",
      "echo 'console.log('dummy app run');' >> testapp.js",
      "sudo pm2 start testapp.js &> pm2start.log",
      "sudo pm2 stop all &> pm2stop.log",
      "sudo pm2 delete all &> pm2delete.log",
      "REGION=$(curl 169.254.169.254/latest/meta-data/placement/availability-zone/ | sed 's/[a-z]$//')",
      "cd /home/ec2-user",
      "wget https://aws-codedeploy-us-east-2.s3.amazonaws.com/latest/install",
      "chmod +x ./install",
      "sudo ./install auto"
    ]

    connection {
      type        = "ssh"
      user        = "ec2-user"
      host        = self.public_ip
      private_key = file(var.private_key_path)
    }
  }

}

resource "aws_instance" "ProductionInstance" {
  ami                  = "ami-f63b1193"
  instance_type        = var.InstanceType
  key_name             = aws_key_pair.terraform_resource_kp.key_name
  iam_instance_profile = aws_iam_instance_profile.DynatraceEC2InstanceProfile.name
  security_groups = [
    aws_security_group.enable_http_ssh.name
  ]
  tags = {
    Environment = "DeployProduction"
    Name        = "Production"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum update -y && sudo yum install -y  httpd ruby wget",
      "wget  -O /tmp/Dynatrace-OneAgent-Linux.sh '${var.DynatraceOneAgentURL}' --header='Authorization: Api-Token ${var.DynatraceOneAgentToken}'",
      "sudo /bin/sh /tmp/Dynatrace-OneAgent-Linux.sh --set-app-log-content-access=true --set-infra-only=false",
      "curl --silent --location https://rpm.nodesource.com/setup_10.x | sudo bash -",
      "sudo yum install -y nodejs",
      "sudo npm install pm2@latest -g",
      "cd /home/ec2-user",
      "export DT_CUSTOM_PROP=DEPLOYMENT_ID=123456 DEPLOYMENT_GROUP_NAME=GROUP_NAME APPLICATION_NAME=APP_NAME",
      "echo 'console.log('dummy app run');' >> testapp.js",
      "sudo pm2 start testapp.js &> pm2start.log",
      "sudo pm2 stop all &> pm2stop.log",
      "sudo pm2 delete all &> pm2delete.log",
      "REGION=$(curl 169.254.169.254/latest/meta-data/placement/availability-zone/ | sed 's/[a-z]$//')",
      "cd /home/ec2-user",
      "wget https://aws-codedeploy-us-east-2.s3.amazonaws.com/latest/install",
      "chmod +x ./install",
      "sudo ./install auto"
    ]

    connection {
      type        = "ssh"
      user        = "ec2-user"
      host        = self.public_ip
      private_key = file(var.private_key_path)
    }
  }

}
